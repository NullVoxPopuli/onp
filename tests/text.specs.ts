import {diffText} from "../src/index";

describe("onp", () => {

	describe("text diff", () => {

		it("check same text", () => {
			const data = diffText("Hello how are you?", "Hello how are you?");

			expect(data.distance).toBe(0);
			expect(data.lcs).toBe("Hello how are you?");
			expect(data.results).toEqual([
				{ data: 'Hello how are you?', state: 0 }
			]);
		});

		it("example test", () => {
			const data = diffText("Test A", "Test B");

			expect(data.distance).toBe(2);
			expect(data.lcs).toBe("Test ");
			expect(data.results).toEqual([
				{ data: 'Test ', state: 0 },
				{ data: 'A', state: -1 },
				{ data: 'B', state: 1 }
			]);
		});

		it("check slightly different text", () => {
			const data = diffText("Hello how are you?", "Hello huw are you?");

			expect(data.distance).toBe(2);
			expect(data.lcs).toBe("Hello hw are you?");
			expect(data.results).toEqual([
				{ data: 'Hello h', state: 0 },
				{ data: 'o', state: -1 },
				{ data: 'u', state: 1 },
				{ data: 'w are you?', state: 0 }
			]);
		});

		it("check more different text with some new words", () => {
			const data = diffText("Hello huw are you?", "Hello man how are you today?");

			expect(data.distance).toBe(12);
			expect(data.lcs).toBe("Hello hw are you?");
			expect(data.results).toEqual([
				{ data: 'Hello ', state: 0 },
				{ data: 'man ', state: 1 },
				{ data: 'h', state: 0 },
				{ data: 'o', state: 1 },
				{ data: 'u', state: -1 },
				{ data: 'w are you', state: 0 },
				{ data: ' today', state: 1 },
				{ data: '?', state: 0 }
			]);
		});

		it("check more totally different text", () => {
			const data = diffText("Hello how are you?", "Today is a good day!");

			expect(data.distance).toBe(26);
			expect(data.lcs).toBe("o o ay");
			expect(data.results).toEqual([
				{data: "T", state: 1},
				{data: "Hell", state: -1},
				{data: "o", state: 0},
				{data: "day", state: 1},
				{data: " ", state: 0},
				{data: "is a g", state: 1},
				{data: "h", state: -1},
				{data: "o", state: 0},
				{data: "od", state: 1},
				{data: "w", state: -1},
				{data: " ", state: 0},
				{data: "d", state: 1},
				{data: "a", state: 0},
				{data: "re ", state: -1},
				{data: "y", state: 0},
				{data: "!", state: 1},
				{data: "ou?", state: -1}
			]);
		});

		it("check 'The men are bad. I hate the men' and 'The men are bad. John likes the men. I hate the men'", () => {
			const data = diffText("The men are bad. I hate the men", "The men are bad. John likes the men. I hate the men");

			expect(data.distance).toBe(20);
			expect(data.lcs).toBe("The men are bad. I hate the men");
			expect(data.results).toEqual([
				{data: "The men are bad. ", state: 0},
				{data: "John likes the men. ", state: 1},
				{data: "I hate the men", state: 0}
			]);
		});

	});

});