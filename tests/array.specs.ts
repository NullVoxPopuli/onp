import {diffArray} from "../src/index";

describe("onp", () => {

	describe("array diff with text", () => {

		it("check same array", () => {
			const data = diffArray(["1", "2", "3"], ["1", "2", "3"]);
			expect(data.distance).toBe(0);
			expect(data.lcs).toEqual(["1", "2", "3"]);
			expect(data.results).toEqual([
				{ data: '1', state: 0 },
				{ data: '2', state: 0 },
				{ data: '3', state: 0 }
			]);
		});

		it("check arrays [1, 2, 3] and [1, 2]", () => {
			const data = diffArray(["1", "2", "3"], ["1", "2"]);
			expect(data.distance).toBe(1);
			expect(data.lcs).toEqual(["1", "2"]);
			expect(data.results).toEqual([
				{ data: '1', state: 0 },
				{ data: '2', state: 0 },
				{ data: '3', state: -1 }
			]);
		});

		it("check arrays [1, 2] and [1, 2, 3]", () => {
			const data = diffArray(["1", "2"], ["1", "2", "3"]);
			expect(data.distance).toBe(1);
			expect(data.lcs).toEqual(["1", "2"]);
			expect(data.results).toEqual([
				{ data: '1', state: 0 },
				{ data: '2', state: 0 },
				{ data: '3', state: 1 }
			]);
		});

	});

	describe("array diff with .toString() objects", () => {

		function obj(id: string) {
			return {
				id: id,
				toString: () => id
			}
		}

		it("check same array", () => {
			const arrayA = [obj("id001"), obj("id002"), obj("id003"), obj("id004")];
			const arrayB = [obj("id001"), obj("id002"), obj("id003"), obj("id004")];

			const data = diffArray(arrayA, arrayB);
			expect(data.distance).toBe(0);
			expect(data.lcs.length).toBe(4);
			expect(data.lcs[0]).toBe(arrayB[0]);
			expect(data.lcs[1]).toBe(arrayB[1]);
			expect(data.lcs[2]).toBe(arrayB[2]);
			expect(data.lcs[3]).toBe(arrayB[3]);

			expect(data.results.length).toBe(4);
			expect(data.results[0].data).toBe(arrayB[0]);
			expect(data.results[0].state).toBe(0);
			expect(data.results[1].data).toBe(arrayB[1]);
			expect(data.results[1].state).toBe(0);
			expect(data.results[2].data).toBe(arrayB[2]);
			expect(data.results[2].state).toBe(0);
			expect(data.results[3].data).toBe(arrayB[3]);
			expect(data.results[3].state).toBe(0);
		});

		it("check array [1, 2, 3, 4] and [1, 4]", () => {
			const arrayA = [obj("id001"), obj("id002"), obj("id003"), obj("id004")];
			const arrayB = [obj("id001"), obj("id004")];

			const data = diffArray(arrayA, arrayB);
			expect(data.distance).toBe(2);
			expect(data.lcs.length).toBe(2);
			expect(data.lcs[0]).toBe(arrayB[0]);
			expect(data.lcs[1]).toBe(arrayB[1]);

			expect(data.results.length).toBe(4);
			expect(data.results[0].data).toBe(arrayB[0]);
			expect(data.results[0].state).toBe(0);
			expect(data.results[1].data).toBe(arrayA[1]);
			expect(data.results[1].state).toBe(-1);
			expect(data.results[2].data).toBe(arrayA[2]);
			expect(data.results[2].state).toBe(-1);
			expect(data.results[3].data).toBe(arrayB[1]);
			expect(data.results[3].state).toBe(0);
		});

		it("check array [1, 2] and [1, 2, 3, 4]", () => {
			const arrayA = [obj("id001"), obj("id004")];
			const arrayB = [obj("id001"), obj("id002"), obj("id003"), obj("id004")];

			const data = diffArray(arrayA, arrayB);
			expect(data.distance).toBe(2);
			expect(data.lcs.length).toBe(2);
			expect(data.lcs[0]).toBe(arrayB[0]);
			expect(data.lcs[1]).toBe(arrayB[3]);

			expect(data.results.length).toBe(4);
			expect(data.results[0].data).toBe(arrayB[0]);
			expect(data.results[0].state).toBe(0);
			expect(data.results[1].data).toBe(arrayB[1]);
			expect(data.results[1].state).toBe(1);
			expect(data.results[2].data).toBe(arrayB[2]);
			expect(data.results[2].state).toBe(1);
			expect(data.results[3].data).toBe(arrayB[3]);
			expect(data.results[3].state).toBe(0);
		});

	});

});