import {createResultItem, ResultItem} from "./results";
import {SES_ADD, SES_COMMON, SES_DELETE} from "./data";

export type CodeMap = {
	pointer: number;
	forward: {
		[key: string]: string;
	},
	backward: {
		[key: string]: string;
	}
}
export interface ComparedItem {
	toString(): string;
}
export function stringifyArray<T extends ComparedItem>(a: Array<T>, b: Array<T>): [string, string, CodeMap] {
	const map: CodeMap = { forward: {}, backward: {}, pointer: 1 };

	const textA = a.map((item) => determineCode(map, item)).join("");
	const textB = b.map((item) => determineCode(map, item)).join("");

	return [textA, textB, map];
}
export function objectifyArray<T extends ComparedItem>(arrayA: Array<T>, arrayB: Array<T>, res: Array<ResultItem<string>>, map: CodeMap): Array<ResultItem<T>> {
	const results = res.map((r) => createResultItem(map.backward[r.data], r.state)) as Array<ResultItem<any>>;

	results.filter((item) => {
		return item.state === SES_COMMON || item.state === SES_DELETE;
	}).forEach((item, index) => {
		item.data = arrayA[index];
	});
	results.filter((item) => {
		return item.state === SES_COMMON || item.state === SES_ADD;
	}).forEach((item, index) => {
		item.data = arrayB[index];
	});

	return results;
}

export function objectifyLcs<T extends ComparedItem>(map: CodeMap, res: Array<ResultItem<T>>): Array<T> {
	return res.filter((item) => {
		return item.state === SES_COMMON;
	}).map((item, index) => {
		return item.data;
	});
}

function determineCode<T extends ComparedItem>(map: CodeMap, item: T): string {
	let id = item.toString();
	let code = map.forward[id];

	if (!code) {
		code = String.fromCharCode(map.pointer);
		map.forward[id] = code;
		map.backward[code] = id;
		map.pointer++;
	}

	return code;
}