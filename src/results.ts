
export type ResultItem<T> = {
	data: T;
	state: -1 | 0 | 1;
}
export function createResultItem<T>(data: T, state: -1 | 0 | 1): ResultItem<T> {
	return { data, state };
}

export type TextResults = Array<ResultItem<string>>;
export function createTextResults(results: Array<ResultItem<string>>): TextResults {

	if (results.length === 0) {
		return [];
	}

	let last = createResultItem(results[0].data, results[0].state);
	let shrink: TextResults = [last];

	results.slice(1).forEach((item) => {
		if (item.state !== last.state) {
			last = createResultItem(item.data, item.state);
			shrink.push(last);
		} else {
			last.data += item.data;
		}
	});

	return shrink;
}

export type ArrayResults<T> = Array<ResultItem<T>>;