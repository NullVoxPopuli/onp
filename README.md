![ONP][logo] ONP
======

[![Pipeline][pipeline]][link-pipeline] 
[![Npm][npm-version]][link-npm]
[![Downloads][npm-downloads]][link-npm]
[![License][license]][link-license]
[![Node][node]][link-node]
[![Collaborators][collaborators]][link-npm]

### What is it?

Text diff algorithm based on 'An O(NP) Sequence Comparison Algorithm' from Sun Wu, Udi Manber and Gene Myers. This 
 is implementation of this algorithm wrapped into typescript and UMD module. This module is also able compare array
 of items with method `.toString()` and keep original objects and instances. It's has really simple API.

### Install

With [npm](https://npmjs.org) do:

```
npm install onp
```

### API for compare

#### `diffText(a: string, b: string):` [`DiffText`][DiffText]

 This method take 2 string and make compare of these strings. It returns object with
  `lcs` property, that is contains ["longest common subsequence"][ext-link-lcs] text, 
  `distance` property that represents ["Levenshtein distance"][ext-link-levenshtein_distance] and
  `results` property that contains array with string and state (1, 0, -1)
  
#### `diffArray<T>(a: Array<T>, b: Array<T>):` [`DiffArray<T>`][DiffArray]

 This method take 2 array of objects, that has implemented method `.toString()` and make 
 compare of these objects based on `.toString()` result. This method try to keep instances of
  objects as valid as possible. It returns object with
  `lcs` property, that is contains ["longest common subsequence"][ext-link-lcs] text, 
  `distance` property that represents ["Levenshtein distance"][ext-link-levenshtein_distance] and
  `results` property that contains array with items of `T` and state (1, 0, -1)

##### Typescript usage example

```typescript
import {diffText} from "onp";
const results = diffText("Text A", "Text B");

console.log(results.distance);
//2
console.log(results.lcs);
//"Text "
console.log(results.results);
//[
// { data: 'Text ', state: 0 },
// { data: 'A', state: -1 },
// { data: 'B', state: 1 } 
//]
```

##### Javascript usage example

```javascript
const diffText = require("onp").diffText;
const results = diffText("Text A", "Text B");

console.log(results.distance);
//2
console.log(results.lcs);
//"Text "
console.log(results.results);
//[
// { data: 'Text ', state: 0 },
// { data: 'A', state: -1 },
// { data: 'B', state: 1 } 
//]
```

```javascript
const results = onp.diffText("Text A", "Text B");

console.log(results.distance);
//2
console.log(results.lcs);
//"Text "
console.log(results.results);
//[
// { data: 'Text ', state: 0 },
// { data: 'A', state: -1 },
// { data: 'B', state: 1 } 
//]
```

### Examples

> Live example with text diff, click on image to run CodePen

[![Text Diff][codepen-text]][codepen-link]


### Donate me 😉

| QR | Paypal |
| ------ | ------ |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |


### License

 MIT - [MIT License](https://spdx.org/licenses/MIT.html)
 
 [DiffText]: https://gitlab.com/stanislavhacker/onp/blob/master/src/index.ts#L14
 [DiffArray]: https://gitlab.com/stanislavhacker/onp/blob/master/src/index.ts#L31
 
 [logo]: https://gitlab.com/stanislavhacker/onp/raw/master/logo.png
 [codepen-text]: https://gitlab.com/stanislavhacker/onp/raw/master/assets/codepen-text.png
 [codepen-link]: https://codepen.io/hackerstanislav/full/wvBPRPX
 
 [pipeline]: https://gitlab.com/stanislavhacker/onp/badges/master/pipeline.svg
 [npm-version]: https://img.shields.io/npm/v/onp.svg
 [npm-downloads]: https://img.shields.io/npm/dm/onp.svg
 [license]: https://img.shields.io/npm/l/onp.svg
 [node]: https://img.shields.io/node/v/onp.svg
 [collaborators]: https://img.shields.io/npm/collaborators/onp.svg
 
 [link-license]: https://gitlab.com/stanislavhacker/onp/blob/master/LICENSE
 [link-npm]: https://www.npmjs.com/package/onp
 [link-pipeline]: https://gitlab.com/stanislavhacker/onp/pipelines
 [link-node]: https://nodejs.org/en/
 
 [ext-link-lcs]: https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
 [ext-link-levenshtein_distance]: https://en.wikipedia.org/wiki/Levenshtein_distance